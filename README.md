# JSClue

## Scripts
#### build
Creates a built version of the application ready to be served.
> npm run build

#### dev
Starts the development server. The development server has real time typescript parsing and linting with hot-reloading.
> npm run dev

#### lint
Lints all files inside the `src` directory with the extensions `.js`, `.jsx`, `.ts`, or `.tsx`
> npm run lint
