import React from 'react';
import { Router } from '@components';
import { useSearch } from '@stores';
import { GlobalStyle, Theme } from './styles';

export const App: React.FC = () => {
  const [searchState] = useSearch();

  return (
    <>
      <GlobalStyle modalOpen={searchState.modalOpen} />
      <Theme>
        <Router />
      </Theme>
    </>
  );
};
