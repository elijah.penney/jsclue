import { createGlobalStyle } from 'styled-components';

interface GlobalStyleType {
  modalOpen: boolean;
}

export const GlobalStyle = createGlobalStyle<GlobalStyleType>`
  html {
    margin: 0;
    padding: 0;
    font-size: 16px;
  }

  body {
    font-size: 16px;
    color: rgba(0, 0, 0, 0.7);
    overflow: ${(props) => (props.modalOpen ? 'hidden' : 'unset')};
  }
`;
