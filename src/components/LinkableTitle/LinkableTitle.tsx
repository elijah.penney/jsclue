import React from 'react';
import { LinkOutlined } from '@ant-design/icons';
import { slugify } from '@helpers';
import { TitleContainer } from './styles';
import { copyLocation } from './helpers';

export const LinkableTitle: React.FC = ({ children }) => {
  const slug = `${slugify(children)}`;

  return (
    <h2>
      <TitleContainer id={slug}>
        <div>{children}</div>
        <a href={`#${slug}`}>
          <LinkOutlined onClick={copyLocation} />
        </a>
      </TitleContainer>
    </h2>
  );
};
