export const copyLocation = () => {
  if (navigator.clipboard) {
    navigator.clipboard.writeText(location.href);
  }
};
