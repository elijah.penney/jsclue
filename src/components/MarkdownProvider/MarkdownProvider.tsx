import React from 'react';
import { MDXProvider } from '@mdx-js/react';
import {
  Blockquote,
  PageTitle,
  LinkableTitle,
  InlineCode,
  CodeBlock,
} from '@components';

export const MarkdownProvider: React.FC = ({ children }) => {
  return (
    <MDXProvider
      components={{
        blockquote: Blockquote,
        h1: PageTitle,
        h2: LinkableTitle,
        inlineCode: InlineCode,
        code: CodeBlock,
      }}
    >
      {children}
    </MDXProvider>
  );
};
