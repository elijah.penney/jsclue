import styled from 'styled-components';
export const BrandHeader = styled.h1`
  color: #ffffff;
  background-color: #333333;
  padding: 0 0.5rem;

  span {
    color: #f0db4f;
    font-weight: bold;
  }
`;
