import styled from 'styled-components';
export const SearchBarContainer = styled.div`
  width: 100%;
  max-width: 55%;

  @media (min-width: 376px) {
    max-width: 40%;
  }
`;
