import React from 'react';
import { RouteComponentProps, Link } from '@reach/router';
import { SearchBar } from '@components';
import { SearchBarContainer, BrandHeader, NavbarContainer } from './styles';

export const Navbar: React.FC<RouteComponentProps> = () => {
  return (
    <NavbarContainer>
      <div>
        <Link to='/'>
          <BrandHeader>
            <span>JS</span> Clue
          </BrandHeader>
        </Link>
      </div>
      <SearchBarContainer>
        <SearchBar />
      </SearchBarContainer>
    </NavbarContainer>
  );
};
