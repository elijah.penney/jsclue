import styled from 'styled-components';
export const Frame = styled.div`
  min-width: 100%;
  width: 100%;
  border: 1px solid ${(props) => props.theme.colors.border};
  background-color: ${(props) => props.theme.colors.background};
  border-radius: ${(props) => props.theme.borderRadius};
  padding: 1rem;
  height: 250px;
  overflow: auto;
  margin-bottom: 1em;
`;
