import React from 'react';
import { Frame } from './styles';

export const ExampleFrame: React.FC = ({ children }) => {
  return (
    <>
      <Frame>{children}</Frame>
    </>
  );
};
