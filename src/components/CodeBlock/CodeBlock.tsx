import React from 'react';
import Highlight from 'react-highlight.js';
import { CodeBlockProps } from '#types';

export const CodeBlock: React.FC<CodeBlockProps> = ({
  className,
  children,
}) => {
  let language = '';

  if (className) {
    [, language] = className.split('-');
  }

  return <Highlight language={language}>{children}</Highlight>;
};
