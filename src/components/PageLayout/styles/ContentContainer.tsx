import styled from 'styled-components';

export const ContentContainer = styled.div`
  max-width: 768px;
  width: 100%;
  padding: 1rem 1rem 0;
`;
