import styled from 'styled-components';

export const ContentSpacer = styled.div`
  display: flex;
  justify-content: space-around;
  margin-bottom: 2rem;
`;
