import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { Navbar } from '@components';
import { ContentContainer, ContentSpacer } from './styles';

export const PageLayout: React.FC<RouteComponentProps> = ({ children }) => {
  return (
    <ContentSpacer>
      <ContentContainer>
        <Navbar />
        {children}
      </ContentContainer>
    </ContentSpacer>
  );
};
