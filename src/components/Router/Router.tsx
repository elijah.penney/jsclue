import React from 'react';
import { Router as ReachRouter } from '@reach/router';
import Loading from '@pages/Loading';
import styled from 'styled-components';

const StyledRouter = styled(ReachRouter)`
  height: 100%;
`;

const AsyncHome = React.lazy(() => import('@pages/Home'));
const AsyncArticle = React.lazy(() => import('@pages/Article'));

export const Router: React.FC = () => {
  return (
    <React.Suspense fallback={<Loading />}>
      <StyledRouter>
        <AsyncHome path='/' />
        <AsyncArticle path='/article/:slug' slug='' />
      </StyledRouter>
    </React.Suspense>
  );
};
