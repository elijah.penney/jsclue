import styled from 'styled-components';

export const SpinWrapper = styled.div`
  & > div > span {
    font-size: 164px;
    color: #333333;
  }
`;
