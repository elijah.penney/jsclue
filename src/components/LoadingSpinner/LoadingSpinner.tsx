import React from 'react';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { SpinWrapper } from './styles';

export const LoadingSpinner: React.FC = () => {
  return (
    <SpinWrapper>
      <Spin indicator={<LoadingOutlined />} />
    </SpinWrapper>
  );
};
