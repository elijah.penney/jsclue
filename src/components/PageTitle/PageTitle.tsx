import React from 'react';
import { TitleWrapper } from './styles';

export const PageTitle: React.FC = ({ children }) => {
  return <TitleWrapper>{children}</TitleWrapper>;
};
