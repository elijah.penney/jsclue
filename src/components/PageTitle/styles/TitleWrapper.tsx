import styled from 'styled-components';

export const TitleWrapper = styled.div`
  color: #222;
  text-align: center;
  margin: 4rem 0;
  font-size: 23px;
  text-transform: uppercase;
  font-weight: 700;

  @media (min-width: 376px) {
    font-size: 28px;
  }

  @media (min-width: 640px) {
    font-size: 34px;
  }
`;
