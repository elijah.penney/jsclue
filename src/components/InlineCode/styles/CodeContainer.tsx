import styled from 'styled-components';

export const CodeContainer = styled.span`
  background-color: ${(props) => props.theme.colors.background};
  padding: 0 0.3em 0.3rem;
`;
