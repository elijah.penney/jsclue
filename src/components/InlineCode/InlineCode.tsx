import React from 'react';
import { CodeContainer } from './styles';

export const InlineCode: React.FC = ({ children }) => {
  return <CodeContainer>{children}</CodeContainer>;
};
