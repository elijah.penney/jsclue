import styled from 'styled-components';

export const LinkContainer = styled.div`
  width: 100%;
  padding: 0.5rem 1rem;
  background-color: ${(props) => props.theme.colors.background};
  border: 2px solid ${(props) => props.theme.colors.border};
  border-radius: ${(props) => props.theme.borderRadius};
  color: #000;
  display: flex;
  font-size: 14px;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-top: 0.75em;
`;
