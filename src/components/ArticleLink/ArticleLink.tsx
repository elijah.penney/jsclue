import React from 'react';
import { Link } from '@reach/router';
import { LinkContainer } from './styles';
import { ArticleMeta } from '#types';

export const ArticleLink: React.FC<ArticleMeta> = ({ name, slug }) => {
  return (
    <Link to={`/article/${slug}`}>
      <LinkContainer>{name}</LinkContainer>
    </Link>
  );
};
