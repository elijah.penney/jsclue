import React from 'react';
import { BlockContent, IndentBlock, BlockContainer } from './styles';

export const Blockquote: React.FC = ({ children }) => {
  return (
    <BlockContainer>
      <IndentBlock />
      <BlockContent>{children}</BlockContent>
    </BlockContainer>
  );
};
