import styled from 'styled-components';
export const IndentBlock = styled.div`
  min-width: 10px;
  max-width: 10px;
  width: 10px;
  background-color: #b8c8dd;
  margin: 0 1rem;
`;
