import { articleMeta } from '@articles';
import { ArticleMeta } from '#types';

export function getResults(keyword: string): Array<ArticleMeta> {
  return articleMeta.filter((meta) =>
    meta.name.toLowerCase().includes(keyword.toLowerCase()),
  );
}
