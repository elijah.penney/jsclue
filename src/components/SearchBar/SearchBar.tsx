import React, { useEffect, useRef } from 'react';
import { Link } from '@reach/router';
import { useSearch } from '@stores';
import {
  SearchOutlined,
  ArrowUpOutlined,
  ArrowDownOutlined,
} from '@ant-design/icons';
import {
  FakeSearchContainer,
  IconBox,
  ModalCover,
  ModalContainer,
  Modal,
  ModalInstructions,
  SearchBox,
  ResultsContainer,
  Result,
} from './styles';

// We need to add in event listners for ESC and /

export const SearchBar: React.FC = () => {
  const [searchState, searchActions] = useSearch();
  const searchRef = useRef(null);

  useEffect(() => {
    if (searchState.modalOpen) {
      searchRef.current.focus();
    }
  }, [searchState.modalOpen]);

  return (
    <>
      <FakeSearchContainer onClick={() => searchActions.setModalOpen(true)}>
        <div>Search</div>
        <IconBox>
          <SearchOutlined />
        </IconBox>
      </FakeSearchContainer>
      {searchState.modalOpen && (
        <>
          <ModalCover onClick={() => searchActions.setModalOpen(false)} />
          <ModalContainer>
            <Modal>
              <ModalInstructions>
                <div>Search for articles</div>
                <div>
                  Use <ArrowUpOutlined /> and <ArrowDownOutlined /> to pick an
                  option
                </div>
                <div>Press ENTER to pick an option</div>
              </ModalInstructions>
              <SearchBox
                onChange={(event) => searchActions.setPhrase(event)}
                onKeyDown={(event) => searchActions.setCursorByEvent(event)}
                value={searchState.phrase}
                ref={searchRef}
              />
              {searchState.results && (
                <ResultsContainer>
                  {searchState.results.map((meta, index) => (
                    <Link
                      key={meta.name}
                      to={`/article/${meta.slug}/${meta.languages[0]}`}
                    >
                      <Result
                        selected={searchState.cursor === index}
                        onMouseOver={() => searchActions.setCursor(index)}
                        onClick={() => searchActions.setModalOpen(false)}
                      >
                        {meta.name}
                      </Result>
                    </Link>
                  ))}
                </ResultsContainer>
              )}
            </Modal>
          </ModalContainer>
        </>
      )}
    </>
  );
};
