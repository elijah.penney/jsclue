import styled from 'styled-components';
export const Modal = styled.div`
  width: 100%;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  padding: 3px 6px 6px;
  border-radius: ${(props) => props.theme.borderRadius};
`;
