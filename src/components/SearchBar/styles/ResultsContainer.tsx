import styled from 'styled-components';
export const ResultsContainer = styled.div`
  width: 100%;
  padding-top: 6px;
  cursor: pointer;
`;
