import styled from 'styled-components';
export const ModalCover = styled.div`
  height: 100vh;
  width: 100vw;
  max-width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.65);
  z-index: 1000;
`;
