import styled from 'styled-components';
export const ModalInstructions = styled.div`
  display: flex;
  font-size: 12px;
  font-weight: 500;
  padding-bottom: 2px;

  & > div:not(:first-of-type) {
    padding-left: 1.25rem;
    display: none;

    @media (min-width: 650px) {
      display: inline-block;
    }
  }
`;
