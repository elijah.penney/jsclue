import styled from 'styled-components';
export const ModalContainer = styled.div`
  max-width: 768px;
  width: calc(100% - 2rem);
  box-sizing: border-box;
  margin-bottom: 0;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1001;
  margin: 0 auto;
  margin-top: 1rem;
`;
