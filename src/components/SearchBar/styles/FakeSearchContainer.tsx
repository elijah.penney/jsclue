import styled from 'styled-components';
export const FakeSearchContainer = styled.div`
  width: 100%;
  height: 50px;
  box-sizing: border-box;
  padding: 4px 12px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid rgba(0, 0, 0, 0.5);
  border-radius: ${(props) => props.theme.borderRadius};
  cursor: text;
`;
