import styled from 'styled-components';
import { ResultProps } from '../types/';

export const Result = styled.div<ResultProps>`
  border-radius: ${(props) => props.theme.borderRadius};
  border: ${(props) =>
    props.selected ? `1px solid ${props.theme.colors.border}` : 'none'};
  background-color: ${(props) =>
    props.selected ? props.theme.colors.background : '#FFFFFF'};
  padding: 4px;
`;
