import styled from 'styled-components';
export const SearchBox = styled.input`
  width: 100%;
  border-radius: ${(props) => props.theme.borderRadius};
  outline: none;
  box-shadow: none;
  border: 1px solid ${(props) => props.theme.colors.border};
  box-sizing: border-box;
  padding: 0 8px;
  font-size: 18px;
  line-height: 50px;
`;
