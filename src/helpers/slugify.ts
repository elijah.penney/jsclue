import React from 'react';

export function slugify(input: React.ReactNode): string {
  const toString = input.toString();
  const lowercase = toString.toLowerCase();
  const allowedChars = lowercase.replace(/[^\w\s$*_+~.()'"!\-:@]+/g, '');
  const withHyphens = allowedChars.replace(/\s/g, '-');
  return withHyphens;
}
