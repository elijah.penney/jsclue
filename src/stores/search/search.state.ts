import { SearchState } from '.';

export const initSearchState: SearchState = {
  phrase: '',
  modalOpen: false,
  results: [],
  cursor: 0,
};
