import React from 'react';
import { ArticleMeta } from '#types';

export type SearchState = {
  phrase: string;
  modalOpen: boolean;
  results: Array<ArticleMeta>;
  cursor: number;
};

export type SearchActions = {
  setPhrase: (phrase: React.ChangeEvent<HTMLInputElement>) => void;
  setModalOpen: (modalOpen: boolean) => void;
  setCursorByEvent: (event: React.KeyboardEvent) => void;
  setCursor: (cursor: number) => void;
};
