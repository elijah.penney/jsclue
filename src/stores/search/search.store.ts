import React from 'react';
import globalHook from 'use-global-hook';
import { initSearchState, searchActions, SearchState, SearchActions } from '.';

export const useSearch = globalHook<SearchState, SearchActions>(
  React,
  initSearchState,
  searchActions,
);
