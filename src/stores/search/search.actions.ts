import React from 'react';
import { navigate } from '@reach/router';
import { Store } from 'use-global-hook';
import { slugify } from '@helpers';
import { articleMeta } from '@articles';
import { ArticleMeta } from '#types';
import { SearchState, SearchActions } from '.';

function getResults(keyword: string): Array<ArticleMeta> {
  return articleMeta.filter((meta) =>
    meta.name.toLowerCase().includes(keyword.toLowerCase()),
  );
}

function resetStore(store: Store<SearchState, SearchActions>) {
  store.setState({
    results: [],
    cursor: 0,
    phrase: '',
    modalOpen: false,
  });
}

function setPhrase(
  store: Store<SearchState, SearchActions>,
  { target: { value: phrase } }: React.ChangeEvent<HTMLInputElement>,
): void {
  store.setState({
    ...store.state,
    phrase,
    results: getResults(phrase),
  });
}

function setModalOpen(
  store: Store<SearchState, SearchActions>,
  modalOpen: boolean,
): void {
  if (!modalOpen) {
    resetStore(store);
    return;
  }

  store.setState({ ...store.state, modalOpen });
}

function setCursorByEvent(
  store: Store<SearchState, SearchActions>,
  event: React.KeyboardEvent,
): void {
  const { cursor, results } = store.state;
  const isUpArrow = event.keyCode === 38;
  const isDownArrow = event.keyCode === 40;
  const isEnter = event.keyCode === 13;

  if (isUpArrow || isDownArrow || isEnter) {
    event.preventDefault();
  }

  if (!results.length) return;

  switch (true) {
    case isEnter:
      resetStore(store);
      navigate(
        `/article/${slugify(results[cursor].section)}/${results[cursor].slug}`,
      );
      break;
    case isUpArrow && cursor !== 0:
      store.setState({ ...store.state, cursor: cursor - 1 });
      break;
    case isDownArrow && cursor < results.length - 1:
      store.setState({ ...store.state, cursor: cursor + 1 });
      break;
  }
}

function setCursor(
  store: Store<SearchState, SearchActions>,
  cursor: number,
): void {
  store.setState({ ...store.state, cursor });
}

export const searchActions = {
  setPhrase,
  setModalOpen,
  setCursorByEvent,
  setCursor,
};
