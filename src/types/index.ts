import { Module } from 'webpack';
import { RouteComponentProps } from '@reach/router';

// Articles

export interface ArticleMeta {
  section: string;
  slug: string;
  name: string;
  languages: Array<string>;
  [key: string]: string | Array<string>; // This is for key signature
}

export interface ArticleModule extends Module, ArticleMeta {
  [key: string]: any;
}

export interface ArticleDictionary {
  [key: string]: string;
}

// Code Block
export interface CodeBlockProps {
  className: string;
}

// Pages
export interface ArticlePageProps extends RouteComponentProps {
  slug: string;
}
