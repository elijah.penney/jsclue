import React from 'react';
import ReactDOM from 'react-dom';
import { App } from '@components';
import './styles/highlight.css';

ReactDOM.render(<App />, document.getElementById('app'));
