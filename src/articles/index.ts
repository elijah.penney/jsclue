import { ArticleMeta, ArticleModule } from '#types';

function getModules(): Array<ArticleModule> {
  const context: any = require.context('./', true, /\index.mdx?/);
  return context.keys().map(context);
}

function getMeta(modules: Array<ArticleModule>): Array<ArticleMeta> {
  return modules.map((mod) => {
    return {
      section: mod.section,
      slug: mod.slug,
      name: mod.name,
      languages: mod.languages,
    };
  });
}

const articleMeta = getMeta(getModules());

export { articleMeta };
