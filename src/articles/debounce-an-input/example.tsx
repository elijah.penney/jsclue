import React, { useState } from 'react';

export const DebounceExample: React.FC = () => {
  const [search, setSearch] = useState<string>('');

  /**
   * Creates a function that debounces the callback by the duration
   * @param duration The amount of time to wait before calling the callback
   * @param callback The function to be debounced
   * @returns A function that debounces the callback by the duration
   */
  function makeDebounce(
    duration = 250,
    callback: (value: string) => void,
  ): (event: React.ChangeEvent<HTMLInputElement>) => void {
    let timeout: number = null;

    /**
     * Sets or clears a timeout that runs a callback with the event value
     * @param event The event from an input change
     */
    function debounce({
      target: { value },
    }: React.ChangeEvent<HTMLInputElement>) {
      if (timeout) clearTimeout(timeout);
      timeout = setTimeout(() => callback(value), duration);
    }

    return debounce;
  }

  const debounce = makeDebounce(500, setSearch);

  return (
    <div>
      <input onChange={debounce} />
      <div>Output: {search}</div>
    </div>
  );
};
