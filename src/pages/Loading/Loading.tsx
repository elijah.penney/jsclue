import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { LoadingSpinner, PageLayout } from '@components';
import { ContentContainer } from './styles';

const Loading: React.FC<RouteComponentProps> = () => {
  return (
    <PageLayout>
      <ContentContainer>
        <LoadingSpinner />
      </ContentContainer>
    </PageLayout>
  );
};

export default Loading;
