import React, { useEffect, useState } from 'react';
import { MarkdownProvider, PageLayout, PageTitle } from '@components';
import { Helmet } from 'react-helmet';
import { getReadableLanguages } from './helpers';
import { unslugify } from '@helpers';
import { Tabs } from 'antd';
import { ArticlePageProps, ArticleMeta, ArticleDictionary } from '#types';
const { TabPane } = Tabs;

const Article: React.FC<ArticlePageProps> = ({ slug }) => {
  const [markdown, setMarkdown] = useState<ArticleDictionary>({});
  const [name, setName] = useState('');

  useEffect(() => {
    const getPage = async () => {
      const meta: ArticleMeta = await import(
        `../../articles/${slug}/index.mdx`
      );

      const articles = meta.languages.map((lang) =>
        import(`../../articles/${slug}/${lang}.mdx`),
      );

      Promise.all(articles).then((results) => {
        setMarkdown(
          results.reduce((acc, cur, index) => {
            const lang = meta.languages[index];
            return {
              ...acc,
              [lang]: cur.default(cur),
            };
          }, {}),
        );

        setName(meta.name);
      });
    };
    getPage();
  }, [slug]);

  return (
    <>
      <Helmet>
        <title>JS Clue | {unslugify(slug)}</title>
      </Helmet>
      <PageLayout>
        {Boolean(Object.keys(markdown).length) && (
          <>
            <PageTitle>{name}</PageTitle>
            <Tabs defaultActiveKey={Object.keys(markdown)[0]} type='card'>
              {Object.keys(markdown).map((lang) => {
                const title = getReadableLanguages(lang);

                return (
                  <TabPane key={lang} tab={title}>
                    <MarkdownProvider>{markdown[lang]}</MarkdownProvider>
                  </TabPane>
                );
              })}
            </Tabs>
          </>
        )}
      </PageLayout>
    </>
  );
};

export default Article;
