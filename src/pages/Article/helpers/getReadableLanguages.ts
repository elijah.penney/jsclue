interface LangDictType {
  [key: string]: string;
}

/**
 * Turns the language string into a full word
 * @param language The language to retrieve a readable form of
 * @returns A readable version of a word
 */
export function getReadableLanguages(language: string) {
  const dict: LangDictType = {
    javascript: 'JavaScript',
    typescript: 'TypeScript',
    react: 'React',
  };

  return dict[language];
}
