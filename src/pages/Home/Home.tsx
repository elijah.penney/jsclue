import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { PageTitle, ArticleLink, PageLayout } from '@components';
import { articleMeta } from '@articles';
import { LinkContainer, SectionTitle, JSBlock } from './styles';
import { Helmet } from 'react-helmet';
import { getSections, getArticlesBySection } from './helpers';

const Home: React.FC<RouteComponentProps> = () => {
  const sections = getSections(articleMeta);

  return (
    <PageLayout>
      <Helmet>
        <title>JS Clue</title>
      </Helmet>
      <PageTitle>
        How to do stuff in <JSBlock>JS</JSBlock>
      </PageTitle>
      {sections.map((section) => {
        const articles = getArticlesBySection(articleMeta, section);

        return (
          <section key={`${section}`}>
            <SectionTitle>{section}</SectionTitle>
            <LinkContainer>
              {articles.map((article) => (
                <ArticleLink
                  key={article.slug}
                  section={section}
                  {...article}
                />
              ))}
            </LinkContainer>
          </section>
        );
      })}
    </PageLayout>
  );
};

export default Home;
