import { ArticleMeta } from '#types';

export function getArticlesBySection(
  articleMeta: Array<ArticleMeta>,
  section: string,
): Array<ArticleMeta> {
  return articleMeta
    .filter((article) => article.section === section)
    .sort((a, b) => (a.name > b.name ? 1 : -1));
}
