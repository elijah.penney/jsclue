import { ArticleMeta } from '#types';

export function getSections(articleMeta: Array<ArticleMeta>): Array<string> {
  return [
    ...new Set(
      articleMeta
        .map(({ section }) => section)
        .sort((a, b) => (a > b ? 1 : -1)),
    ),
  ];
}
