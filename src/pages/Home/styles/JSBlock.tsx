import styled from 'styled-components';
export const JSBlock = styled.span`
  background-color: #333;
  color: #f0db4f;
  padding: 0 0.5rem;
`;
