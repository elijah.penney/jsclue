import styled from 'styled-components';
export const SectionTitle = styled.h2`
  text-transform: uppercase;
  font-size: 24px;
  font-weight: 700;
  margin-top: 1rem;
`;
